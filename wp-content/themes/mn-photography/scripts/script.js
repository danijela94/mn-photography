jQuery(document).ready(function ($) {
	$.fn.isInViewport = function () {
		var elementTop = $(this).offset().top;
		var elementBottom = elementTop + $(this).outerHeight();

		var viewportTop = $(window).scrollTop();
		var viewportBottom = viewportTop + $(window).height();

		return elementBottom > viewportTop && elementTop < viewportBottom;
	};
	$(window).on('load', function () {
		$('nav .item a').each(function () {
			var hash = $(this).attr("href");
			if ($(hash).isInViewport()) {
				$('a[href="' + hash + '"]').closest('li').addClass('active');
			} else {
				$('a[href="' + hash + '"]').closest('li').removeClass('active');
			}
		})
	});
	//Smooth scrolling effect
	$("a").on('click', function (event) {
		if (this.hash !== "") {
			event.preventDefault();
			var hash = this.hash;
			$('html, body').animate({
				scrollTop: $(hash).offset().top
			});
		}
	});
	$(window).on('scroll', function () {
		var scrollPos = $(document).scrollTop();
		$('nav .item a').each(function () {
			var currLink = $(this);
			var refElement = $(currLink.attr("href"));
			if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
				$('nav .main-nav .item a').removeClass("active");
				currLink.parent().addClass("active");
			}
			else {
				currLink.parent().removeClass("active");
			}
		});
	});


})