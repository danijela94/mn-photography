<!DOCTYPE html>
<html <?php language_attributes();?>>
	<head>
		<meta charset ="<?php bloginfo('charset');?>"/>
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<?php wp_head(); ?>
	</head>
<body <?php body_class(); ?>>
<?php 

global $wp_query, $logo, $logo_img_src, $post; 

$custom_logo_id = get_theme_mod('custom_logo');
$logo = wp_get_attachment_image_src($custom_logo_id, 'full');
$logo_img_src = esc_url( $logo[0] );

$menu_name = 'headerMenuLocation';
$locations = get_nav_menu_locations();
$menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
$menuitems = wp_get_nav_menu_items( $menu->term_id, array( 'order' => 'DESC' ) );
$count = 0;
$submenu = false;

?>
<nav>
	<ul class="main-nav">
		<?php if($logo): ?>
		<li class="logo">
			<a href="<?php echo site_url(); ?>"><img src="<?php echo $logo_img_src; ?>" /></a>
		</li>
		<?php endif; ?>
		<?php
		$count = 0;
		$submenu = false;
		foreach( $menuitems as $item ):
			$link = $item->url;
			$title = $item->title;
			// item does not have a parent so menu_item_parent equals 0 (false)
			//if ( !$item->menu_item_parent ):

				// save this id for later comparison with sub-menu items
				//$parent_id = $item->ID;
			?>
			
			<li class="item">
				<a href="#<?php echo strtolower($title); ?>" class="title">
					<?php echo $title; ?>
				</a>
			<?php //endif; ?>

				<?php //if ( $parent_id == $item->menu_item_parent ): ?>

					<?php //if ( !$submenu ): $submenu = true; ?>
					<!-- <ul class="sub-menu"> -->
					<?php //endif; ?>

						<!-- <li class="item">
							<a href="<?php //echo $link; ?>" class="title"><?php //echo $title; ?></a>
						</li> -->

					<?php //if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id && $submenu ): ?>
					<!-- </ul> -->
					<?php// $submenu = false; endif; ?>

				<?php//endif; ?>

			<?php //if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id ): ?>
			</li>                           
			<?php// $submenu = false; endif; ?>

		<?php //$count++; 
		endforeach; ?>
		</ul>
</nav>
			
