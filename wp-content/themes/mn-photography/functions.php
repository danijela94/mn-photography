<?php
add_action('wp_enqueue_scripts', 'mn_scriptFiles');
add_action('after_setup_theme', 'mn_themeFeatures');

function mn_scriptFiles(){
	wp_enqueue_style('main_styles', get_stylesheet_uri());
	//wp_enqueue_style('font-awesome', '//use.fontawesome.com/releases/v5.8.2/css/all.css');
	wp_enqueue_script('main_scripts', get_stylesheet_directory_uri() . '/scripts/script.js', array('jquery') );
	//wp_enqueue_script('nav_script', get_stylesheet_directory_uri() . '/scripts/navigation.js', array('jquery') );
}

function mn_themeFeatures(){
	add_theme_support('custom-logo');
	register_nav_menu('headerMenuLocation', 'Header Menu Location');
	add_theme_support('post-thumbnails');
}

