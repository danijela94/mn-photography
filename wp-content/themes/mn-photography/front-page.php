<?php get_header(); ?>

<?php 
//About page
$page_about = get_page_by_path( 'about' );
$title_about = get_the_title($page_about);
$content_about = get_the_content(null, null, $page_about);
$thumb_url_about = get_the_post_thumbnail_url($page_about->ID);

//Contact page
$page_contact = get_page_by_path( 'contact' );
$title_contact = get_the_title($page_contact);
$content_contact = get_the_content(null, null, $page_contact);
$thumb_url_contact = get_the_post_thumbnail_url($page_contact->ID);
?>

<div class="container-fluid" id="home">
	<div class="row">
		<div class="col-2">
		</div>
		<div class="col-10" style="height: 100vh;">

		</div>
	</div>
</div>
<div class="container-fluid" id="about">
	<div class="row">
		<div class="col-2">
		</div>
		<div class="col-10">
			<div class="page-container">
				<div class="title">
					<style>
						.mn-img{
							background-image: url(<?php echo $thumb_url_about; ?>);
						}
					</style>
					<div class="mn-img">
					</div>
					<h1><?php echo $title_about; ?></h1>
				</div>
				<div class="desc">
					<?php echo $content_about; ?>
				</div>
			</div>	
		</div>
	</div>
</div>
<div class="container-fluid" id="contact">
	<div class="row">
		<div class="col-2">
		</div>
		<div class="col-10">
			<div class="page-container">
				<div class="title">
					<h1><?php echo $title_contact; ?></h1>
				</div>
				<div class="desc">
					<?php echo $content_contact; ?>
				</div>
			</div>	
		</div>
	</div>
</div>
<?php get_footer(); ?>